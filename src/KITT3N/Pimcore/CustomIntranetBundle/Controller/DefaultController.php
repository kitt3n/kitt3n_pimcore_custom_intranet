<?php

namespace KITT3N\Pimcore\CustomIntranetBundle\Controller;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends FrontendController
{
    public function c12Action(Request $request)
    {
        $this->enableViewAutoRender($request, 'twig');
    }

    public function c9c3Action(Request $request)
    {
        $this->enableViewAutoRender($request, 'twig');
    }

    public function c4c4c4Action(Request $request)
    {
        $this->enableViewAutoRender($request, 'twig');
    }

    public function dashboardAction(Request $request)
    {
        $this->enableViewAutoRender($request, 'twig');
    }
}
