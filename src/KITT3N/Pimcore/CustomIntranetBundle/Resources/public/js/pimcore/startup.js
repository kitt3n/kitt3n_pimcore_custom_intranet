pimcore.registerNS("pimcore.plugin.Kitt3nPimcoreCustomIntranetBundle");

pimcore.plugin.Kitt3nPimcoreCustomIntranetBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.Kitt3nPimcoreCustomIntranetBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("Kitt3nPimcoreCustomIntranetBundle ready!");
    }
});

var Kitt3nPimcoreCustomIntranetBundlePlugin = new pimcore.plugin.Kitt3nPimcoreCustomIntranetBundle();
