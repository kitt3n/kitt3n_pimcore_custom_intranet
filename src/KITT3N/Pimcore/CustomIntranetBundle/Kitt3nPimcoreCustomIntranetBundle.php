<?php

namespace KITT3N\Pimcore\CustomIntranetBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class Kitt3nPimcoreCustomIntranetBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/kitt3npimcorecustomintranet/js/pimcore/startup.js'
        ];
    }
}