/**
 * Includes
 */
var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
// var uglify = require('gulp-uglify');
// var wrap = require('gulp-wrap');
// var tap = require('gulp-tap');
// var util = require('gulp-util');
// var debug = require('gulp-debug');
// var path = require('path');

var bundle = "CustomIntranetBundle";
var namespace = "KITT3N";

/**
 * Scss => Css
 */
gulp.task('viewCss', function () {
    gulp.src(
        [
            'src/' + namespace + '/Pimcore/' +  bundle + '/Resources/private/assets/view/scss/*.scss'
        ]
    )
        .pipe(
            concat('view.css'))
        .pipe(sass({
            paths: ['web']
        }).on('error', sass.logError))
        .pipe(
            gulp.dest(
                'src/' + namespace + '/Pimcore/' +  bundle + '/Resources/public/assets/view/css/'
            )
        )
        .pipe(
            rename('view.min.css')
        )
        .pipe(
            autoprefixer({
                browsers: ['last 3 versions', 'safari 7', 'safari 8', 'edge 15', 'ie 9-11'],
                grid: true
            }))
        .pipe(cleanCSS(
            {
                level: {
                    2: {
                        // mergeAdjacentRules: false, // controls adjacent rules merging; defaults to true
                        // mergeIntoShorthands: false, // controls merging properties into shorthands; defaults to true
                        mergeMedia: false, // controls `@media` merging; defaults to true
                        mergeNonAdjacentRules: false // controls non-adjacent rule merging; defaults to true
                        // mergeSemantically: true // controls semantic merging; defaults to false
                        // overrideProperties: true, // controls property overriding based on understandability; defaults to true
                        // reduceNonAdjacentRules: true, // controls non-adjacent rule reducing; defaults to true
                        // removeDuplicateFontRules: true, // controls duplicate `@font-face` removing; defaults to true
                        // removeDuplicateMediaBlocks: true, // controls duplicate `@media` removing; defaults to true
                        // removeDuplicateRules: true, // controls duplicate rules removing; defaults to true
                        // restructureRules: true // controls rule restructuring; defaults to false
                    }

                }
            }
        ))
        .pipe(
            gulp.dest(
                'src/' + namespace + '/Pimcore/' +  bundle + '/Resources/public/assets/view/css/'
            )
        )
        .pipe(
            gulp.dest(
                '../../../web/bundles/hechingerpimcoreintranet/assets/view/css/'
            )
        )
});

/**
 * Scss => Css
 */
gulp.task('editCss', function () {
    gulp.src(
        [
            'src/' + namespace + '/Pimcore/' +  bundle + '/Resources/private/assets/edit/scss/*.scss'
        ]
    )
        .pipe(
            concat('edit.css'))
        .pipe(sass().on('error', sass.logError))
        .pipe(
            gulp.dest(
                'src/' + namespace + '/Pimcore/' +  bundle + '/Resources/public/assets/edit/css/'
            )
        )
        .pipe(
            rename('edit.min.css')
        )
        .pipe(
            autoprefixer({
                browsers: ['last 3 versions', 'safari 7', 'safari 8', 'edge 15', 'ie 9-11'],
                grid: true
            }))
        .pipe(cleanCSS(
            {
                level: {
                    2: {
                        // mergeAdjacentRules: false, // controls adjacent rules merging; defaults to true
                        // mergeIntoShorthands: false, // controls merging properties into shorthands; defaults to true
                        mergeMedia: false, // controls `@media` merging; defaults to true
                        mergeNonAdjacentRules: false // controls non-adjacent rule merging; defaults to true
                        // mergeSemantically: true // controls semantic merging; defaults to false
                        // overrideProperties: true, // controls property overriding based on understandability; defaults to true
                        // reduceNonAdjacentRules: true, // controls non-adjacent rule reducing; defaults to true
                        // removeDuplicateFontRules: true, // controls duplicate `@font-face` removing; defaults to true
                        // removeDuplicateMediaBlocks: true, // controls duplicate `@media` removing; defaults to true
                        // removeDuplicateRules: true, // controls duplicate rules removing; defaults to true
                        // restructureRules: true // controls rule restructuring; defaults to false
                    }

                }
            }
        ))
        .pipe(
            gulp.dest(
                'src/' + namespace + '/Pimcore/' +  bundle + '/Resources/public/assets/edit/css/'
            )
        )
});

/**
 * Fly Willy fly ;)
 */
gulp.task(
    'default',
    [
        'viewCss',
        'editCss'
    ],
    function () {
        gulp.watch(
            [
                'src/' + namespace + '/Pimcore/' +  bundle + '/Resources/private/assets/view/scss/*.scss',
                'src/' + namespace + '/Pimcore/' +  bundle + '/Resources/private/assets/view/scss/**/*.scss',
            ], ['viewCss'])
            .on('change', function (evt) {
                console.log(
                    '[watcher] File ' + evt.path + ' was ' + evt.type + ', compiling...'
                );
            });
        gulp.watch(
            [
                'src/' + namespace + '/Pimcore/' +  bundle + '/Resources/private/assets/edit/scss/*.scss',
                'src/' + namespace + '/Pimcore/' +  bundle + '/Resources/private/assets/edit/scss/**/*.scss',
            ], ['editCss'])
            .on('change', function (evt) {
                console.log(
                    '[watcher] File ' + evt.path + ' was ' + evt.type + ', compiling...'
                );
            });
    }
);